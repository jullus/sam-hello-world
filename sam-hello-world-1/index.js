const aws = require('aws-sdk');
aws.config.update({region: 'eu-west-1'});
const sns = new aws.SNS()
exports.handler = async function(event, context) {
  const params = {
    Message: 'FMM TBH Hello World!',
    Subject: 'SNS Notification from Lambda',
    TopicArn: process.env.SNS_TOPIC_ARN
  };
  return { statusCode: 200, body: 'MessageFMM TBH' };
};